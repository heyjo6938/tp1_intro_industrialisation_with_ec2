
resource "aws_key_pair" "tp1_intro" {
  key_name   = "tp1_intro"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCu2YiCqsaVsvNJqK8WJj6IOk8hAvts3TPGLGIQ1R0ppDqkXxDoOVKVkh/cXwTOgHgrV2yRXLhYy+DN2MpRqnvcxM5ZmENoy2fE+lqSM+RT+C1qEQvlTMmC/+GRoj8pjTYEc4thjaB0cSwxkvc3VYjA90DW/MDrBaDHIeAXAbYZK4CCfAQ0pJOTdmdR7pIvpOqFxgtMPi/CAqlEgkqGgnHV4jFA8CUGkLqEfmnwO5WghCP9bDvDbAXAjrgD7AeAhh7NH04OsTt3/gfcVbJ6LaNh54DZLd3X5aoaG48eUq7F81RaZ99kpKj2hKtu1Or5D6IU+4x36F8RarMfG6+GHVrub6Kqsetqa2MSQj3LNAMxlQmOTDUO9XKO7iJn41gHa3n3Ig/YrYwHHtpN5ItJUTMWvQeCEW7+3kR5oNE7gy0S53itPJBO6Td3Lsltoun0iozsox/eeKlGarEN75uOOE6Yt+S9JF+luTVyMr1wesKzZv59jiHgkH+Kate8VDpwjKU= jean-paul.elias@esme.fr"
}



resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}
resource "aws_instance" "ec2_instance" {
  ami = var.ami_id
  instance_type = var.instance_type
}


resource "aws_default_security_group" "default" {
  vpc_id = aws_default_vpc.default.id

  ingress {
    protocol  = "tcp"
    self      = true
    from_port = 22
    to_port   = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

    ingress {
    protocol  = "tcp"
    self      = true
    from_port = 80
    to_port   = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }
  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }
}


resource "aws_instance" "web" {
  ami           = var.ami_id
  instance_type = "t2.micro"

  tags = {
    Name = "HelloWorld"
  }
}